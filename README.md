# TTM00TEST_Luis-Antonio-Rodriguez_20220608

Display LCD with TouchScreen

Author.
Luis Antonio Estupiñan Rodriguez

the project consists of a state machine from a previous activity, the statuses are displayed on the board screen, it is designed using touch gfx

The values of the actuators are: 

Heater --> 1: ON / 0:OFF
Door   --> 1: Close / 0: Open
Cooler --> 0 (OFF), 20%(ON), 60% (ON) and 100% (ON).
Temp: shows the temperature
The states-Machine is the next: 

the temperature is greater than 12°C and less than 18°C, the heater and the fan are OFF. the indicator turns green(COLD)
the temperature is greater than 20°C and less than 28°C, the heater is OFF and the fan is at 20% of the speed. the indicator turns green (MILD)
the temperature is greater than 30°C and less than 38°C, the heater is OFF and the fan is at 60% of the speed. the indicator turns orange(HOT)
the temperature is greater than 40°C and less than 45°C, the heater is OFF and the fan is at 100% of the speed.the indicator turns orange(VERY HOT)
the temperature is greater than 45°C or less than 0°C, the heater and the fan are OFF. And show an image (ALERT) on the display LCD.
if the image is pushed the door is opened and if pushed again the door is closed 