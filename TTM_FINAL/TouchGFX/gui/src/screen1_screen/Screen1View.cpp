#include <gui/screen1_screen/Screen1View.hpp>
#include "main.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


Screen1View::Screen1View()
{
	door_open.setVisible(false);
	door_open.invalidate();
	warning.setVisible(false);
	warning.invalidate();
}

void Screen1View::setupScreen()
{
    Screen1ViewBase::setupScreen();
}

void Screen1View::tearDownScreen()
{
    Screen1ViewBase::tearDownScreen();
}

void Screen1View::open_door()
{
	door = !door;

	if( door == 1){
			HAL_GPIO_WritePin(door_GPIO_Port,door_Pin, GPIO_PIN_RESET);
		}else if(door == 0){
			HAL_GPIO_WritePin(door_GPIO_Port,door_Pin, GPIO_PIN_SET);
		}

	Unicode::snprintf(door_statusBuffer,DOOR_STATUS_SIZE,"%d",door);
	door_status.resizeToCurrentText();
	door_status.invalidate();
	door_open.setVisible(door);
	door_open.invalidate();
}

void Screen1View::updateTemp() {

	//float temperature_to_update = get_temperature();
	//float temperature_to_update = 36.00;
	float Temperature_Degrees;
	Temperature_Degrees = get_temperature();

	temp_status.resizeToCurrentText();
	temp_status.invalidate();
	Unicode::snprintfFloat(temp_statusBuffer, TEMP_STATUS_SIZE, "%0.2f", Temperature_Degrees);
	temp_status.resizeToCurrentText();
	temp_status.invalidate();


	if ((Temperature_Degrees >= 45) || (Temperature_Degrees <= 0)) {
		temp_ok.setVisible(false);
		temp_ok.invalidate();
		temp_middle.setVisible(false);
		temp_middle.invalidate();
		temp_not_ok.setVisible(true);
		temp_not_ok.invalidate();
		warning.setVisible(true);
		warning.invalidate();


	} else if((Temperature_Degrees >= 12) || (Temperature_Degrees <= 28)) {

		temp_ok.setVisible(true);
		temp_ok.invalidate();
		temp_middle.setVisible(false);
		temp_middle.invalidate();
		temp_not_ok.setVisible(false);
		temp_not_ok.invalidate();
		warning.setVisible(false);
		warning.invalidate();

	}

	if (Temperature_Degrees < 10.0 && Temperature_Degrees > 0.0) {
		HAL_GPIO_WritePin(heater_GPIO_Port, heater_Pin, GPIO_PIN_SET);
		heater = 1;
		temp_ok.setVisible(false);
		temp_ok.invalidate();
		temp_middle.setVisible(true);
		temp_middle.invalidate();
		temp_not_ok.setVisible(false);
		temp_not_ok.invalidate();
		warning.setVisible(false);
		warning.invalidate();
		Unicode::snprintf(heater_statusBuffer, HEATER_STATUS_SIZE, "%d",heater);
		heater_status.resizeToCurrentText();
		heater_status.invalidate();

	} else if(Temperature_Degrees>10){
		HAL_GPIO_WritePin(heater_GPIO_Port, heater_Pin, GPIO_PIN_RESET);
		heater = 0;
		Unicode::snprintf(heater_statusBuffer, HEATER_STATUS_SIZE, "%d",heater);
		heater_status.resizeToCurrentText();
		heater_status.invalidate();
	}

	if(Temperature_Degrees < 28.0 && Temperature_Degrees > 20.0) {
		pwm_dma(0,20);
		state_cooler=20;
		Unicode::snprintf(cooler_statusBuffer,COOLER_STATUS_SIZE,"%d",state_cooler);
		cooler_status.resizeToCurrentText();
		cooler_status.invalidate();
	}else if(Temperature_Degrees < 38.0 && Temperature_Degrees > 30.0){
		temp_ok.setVisible(false);
		temp_ok.invalidate();
		temp_middle.setVisible(true);
		temp_middle.invalidate();
		temp_not_ok.setVisible(false);
		temp_not_ok.invalidate();
		warning.setVisible(false);
		warning.invalidate();
		pwm_dma(state_cooler,60);
		state_cooler=60;
		Unicode::snprintf(cooler_statusBuffer,COOLER_STATUS_SIZE,"%d",state_cooler);
		cooler_status.resizeToCurrentText();
		cooler_status.invalidate();
	}else if(Temperature_Degrees < 45.0 && Temperature_Degrees > 40.0){
			temp_ok.setVisible(false);
			temp_ok.invalidate();
			temp_middle.setVisible(true);
			temp_middle.invalidate();
			temp_not_ok.setVisible(false);
			temp_not_ok.invalidate();
			warning.setVisible(false);
			warning.invalidate();
			pwm_dma(state_cooler,100);
			state_cooler=100;
			Unicode::snprintf(cooler_statusBuffer,COOLER_STATUS_SIZE,"%d",state_cooler);
			cooler_status.resizeToCurrentText();
			cooler_status.invalidate();
		}else{
			HAL_GPIO_WritePin(cooler_GPIO_Port, cooler_Pin, GPIO_PIN_RESET);
			state_cooler=0;
			Unicode::snprintf(cooler_statusBuffer,COOLER_STATUS_SIZE,"%d",state_cooler);
			cooler_status.resizeToCurrentText();
			cooler_status.invalidate();
		}


}
