#include <gui/model/Model.hpp>
#include <gui/model/ModelListener.hpp>
#include <main.h>
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

extern "C"
{
	xQueueHandle	messageQ;
}


Model::Model() : modelListener(0)
{
	messageQ = xQueueGenericCreate(1, 1, 0);
}

void Model::tick()
{
	float temperature_degrees = get_temperature();
	if(xQueueReceive(messageQ, &temperature_degrees, 0) == pdTRUE){
			modelListener->setNewTemp();
		}
}
