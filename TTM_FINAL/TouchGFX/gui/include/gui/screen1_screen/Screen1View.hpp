#ifndef SCREEN1VIEW_HPP
#define SCREEN1VIEW_HPP

#include <gui_generated/screen1_screen/Screen1ViewBase.hpp>
#include <gui/screen1_screen/Screen1Presenter.hpp>
#include <stdio.h>

class Screen1View : public Screen1ViewBase
{
    public:
        uint8_t door = 0;
        uint8_t heater = 0;
        uint8_t state_cooler = 0;

        Screen1View();
        virtual ~Screen1View() {}
        virtual void setupScreen();
        virtual void tearDownScreen();
        virtual void open_door();
        void updateTemp();
    protected:
};

#endif // SCREEN1VIEW_HPP
