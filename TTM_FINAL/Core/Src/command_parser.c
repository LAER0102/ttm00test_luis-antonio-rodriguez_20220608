#include "command_parser.h"
#include "uart_driver.h"
#include "main.h"
#include <string.h>


#define DATATIME_LEN 11
#define ticks_in_a_sec 1000
#define secs_in_a_min 60
#define mins_in_a_hour 60
#define hours_in_a_day 24
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart3;
extern SPI_HandleTypeDef hspi1;
extern TIM_HandleTypeDef htim2;
extern DMA_HandleTypeDef hdma_tim2_ch1;
/////////////COMMANDS///////////////////
extern uart_driver_t uart_driver;
const uint8_t update_request_cmd[] = "update_request";
const uint8_t example_1_cmd[] = "example_1";
const uint8_t example_2_cmd[] = "example_2";
const uint8_t read_temperature_cmd[] = "READ_TEMPERATURE";
const uint8_t read_fan_speed_cmd[] = "READ_FAN_SPEED";
const uint8_t set_fan_speed_cmd[] = "SET_FAN_SPEED";
const uint8_t close_door_cmd[] = "CLOSE_DOOR";
const uint8_t open_door_cmd[] = "OPEN_DOOR";
const uint8_t get_door_position_cmd[] = "GET_DOOR_POSITION";
const uint8_t get_fw_version_cmd[] = "GET_FW_VERSION";
const uint8_t get_unit_tick_cmd[] = "GET_UNIT_TICK";
const uint8_t get_heater_state_cmd[] = "GET_HEATER_STATE";




////////////////MESSAGES/////////////
const uint8_t ack_message[] = "*ACK#";
const uint8_t temp_message[] = "*T19.29#";
const uint8_t example1_message[] = "*T35.11#*D1#*H0#*F060#";
const uint8_t example2_message[] = "*T05.35#*D0#*H1#*F000#";
const uint8_t state_door_open[] = "*D0#";
const uint8_t state_door_close[] = "*D1#";
const uint8_t state_fan_on[] = "*F1#";
const uint8_t state_fan_off[] = "*F0#";
const uint8_t ack_message6[] = "*V1.0.20220605#";
const uint8_t state_fan_0[] = "*F000#";
const uint8_t state_fan_25[] = "*F025#";
const uint8_t state_fan_50[] = "*F050#";
const uint8_t state_fan_75[] = "*F075#";
const uint8_t state_fan_100[] = "*F100#";
const uint8_t state_heater_on[] = "*H1#";
const uint8_t state_heater_off[] = "*H0#";
const uint8_t nack_message[] = "NACK";


uint8_t cont_fan=0;
uint8_t pwmstate=0;
float temperature;
const uint8_t preamble1[] = "*CCC";
const uint8_t preamble[] = "*T";
const uint8_t posamble[] = "#";




void parse_command(uint8_t *rx_packet )
{
	if(memcmp(rx_packet, read_temperature_cmd, sizeof (read_temperature_cmd)-1)==0){
	/// LO QUE ME DEBE HACER
		//uart_driver_send(&uart_driver,(uint8_t *)preamble, sizeof(preamble)-1);

		//float temperature = BMP280_read_temperature();
		uint8_t temperature_sensor[6];
		//sprintf(temperature_sensor, "%f", temperature);
		//temperature_sensor="19.29";
		//uart_driver_send(&uart_driver,(uint8_t *)preamble, sizeof(preamble)-1);
		//uart_driver_send(&uart_driver,(uint8_t *)temperature_sensor, sizeof(temperature_sensor)-1);
		uart_driver_send(&uart_driver,(uint8_t *)temperature_sensor, sizeof(temperature_sensor)-1);
		//HAL_UART_Transmit(&huart1, temperature, sizeof(temperature), 10);
		//uart_driver_send_command(&uart_driver,(uint8_t *)temperature_sensor, sizeof(temperature_sensor)-1);

	}else if (memcmp(rx_packet, example_1_cmd, sizeof (example_1_cmd)-1)==0){

		HAL_UART_Transmit(&huart1, &example1_message, sizeof(example1_message), 10);
		HAL_UART_Transmit(&huart3, &example1_message, sizeof(example1_message), 10);

	}else if (memcmp(rx_packet, example_2_cmd, sizeof (example_2_cmd)-1)==0){

		HAL_UART_Transmit(&huart1, &example2_message, sizeof(example2_message), 10);
		HAL_UART_Transmit(&huart3, &example2_message, sizeof(example2_message), 10);

	}else if (memcmp(rx_packet, update_request_cmd, sizeof (update_request_cmd)-1)==0){
		char  datos[22]={0};
		///temperature///
		//float temperature = BMP280_read_temperature();
		//uint8_t temperature_sensor[6];
		//sprintf(temperature_sensor, "%f", temperature);
		//temperature_sensor=19.30;
		//uart_driver_send(&uart_driver,(uint8_t *)preamble, sizeof(preamble)-1);
		//uart_driver_send(&uart_driver,(uint8_t *)temperature_sensor, sizeof(temperature_sensor)-1);
		//HAL_UART_Transmit(&huart1,temp_message , sizeof(temp_message), 10);
		//uart_driver_send(&uart_driver,(uint8_t *)posamble, sizeof(posamble)-1);
		strcat(datos,temp_message);
		//////door//////
		if(HAL_GPIO_ReadPin(door_GPIO_Port, door_Pin)==1){
			//uart_driver_send(&uart_driver,(uint8_t *)state_door_open, sizeof(state_door_open)-1);
			strcat(datos,state_door_open);
			//HAL_UART_Transmit(&huart1,state_door_open , sizeof(state_door_open), 10);
		}
		if(HAL_GPIO_ReadPin(door_GPIO_Port, door_Pin)==0)
		{
			//uart_driver_send(&uart_driver,(uint8_t *)state_door_close, sizeof(state_door_close)-1);
			strcat(datos,state_door_close);
			//HAL_UART_Transmit(&huart1,state_door_close , sizeof(state_door_close), 10);

		}

		//////heater State///////
		if(HAL_GPIO_ReadPin(heater_GPIO_Port, heater_Pin)==1){
			//uart_driver_send(&uart_driver,(uint8_t *)state_heater_on, sizeof(state_heater_on)-1);
			strcat(datos,state_heater_on);
			//HAL_UART_Transmit(&huart1, state_heater_on, sizeof(state_heater_on), 10);
		}
		if(HAL_GPIO_ReadPin(heater_GPIO_Port, heater_Pin)==0)
		{
			//uart_driver_send(&uart_driver,(uint8_t *)state_heater_off, sizeof(state_heater_off)-1);
			strcat(datos,state_heater_off);
		}


		/////fan speed///
		switch (pwmstate){
			case 0:

				//uart_driver_send(&uart_driver,(uint8_t *)state_fan_0, sizeof(state_fan_0)-1);
				strcat(datos,state_fan_0);
				//HAL_UART_Transmit(&huart1,state_fan_0 , sizeof(state_fan_0), 10);
				break;
			case 25:
				//uart_driver_send(&uart_driver,(uint8_t *)state_fan_25, sizeof(state_fan_25)-1);
				strcat(datos,state_fan_25);
				//HAL_UART_Transmit(&huart1,state_fan_25 , sizeof(state_fan_25), 10);
				break;
			case 50:
				uart_driver_send(&uart_driver,(uint8_t *)state_fan_50, sizeof(state_fan_50)-1);
				strcat(datos,state_fan_50);
				//HAL_UART_Transmit(&huart1,state_fan_50 , sizeof(state_fan_50), 10);
				break;
			case 75:
				//uart_driver_send(&uart_driver,(uint8_t *)state_fan_75, sizeof(state_fan_75)-1);
				strcat(datos,state_fan_75);
				//HAL_UART_Transmit(&huart1,state_fan_75 , sizeof(state_fan_75), 10);
				break;
			case 100:
				//uart_driver_send(&uart_driver,(uint8_t *)state_fan_100, sizeof(state_fan_100)-1);
				strcat(datos,state_fan_100);
				//HAL_UART_Transmit(&huart1,state_fan_100 , sizeof(state_fan_100), 10);
				break;
			}
		HAL_UART_Transmit(&huart1, &datos, sizeof(datos), 10);
		HAL_UART_Transmit(&huart3, &datos, sizeof(datos), 10);

		////////////////////////////////////////////////

	}else if (memcmp(rx_packet, read_fan_speed_cmd, sizeof (read_fan_speed_cmd)-1)==0){

		switch (pwmstate){
		case 0:
			uart_driver_send(&uart_driver,(uint8_t *)state_fan_0, sizeof(state_fan_0)-1);

			break;
		case 25:
			uart_driver_send(&uart_driver,(uint8_t *)state_fan_25, sizeof(state_fan_25)-1);
			break;
		case 50:
			uart_driver_send(&uart_driver,(uint8_t *)state_fan_50, sizeof(state_fan_50)-1);
			break;
		case 75:
			uart_driver_send(&uart_driver,(uint8_t *)state_fan_75, sizeof(state_fan_75)-1);
			break;
		case 100:
			uart_driver_send(&uart_driver,(uint8_t *)state_fan_100, sizeof(state_fan_100)-1);
			break;
		}

	}else if (memcmp(rx_packet, set_fan_speed_cmd, sizeof (set_fan_speed_cmd)-1)==0){

		cont_fan++;
		switch (cont_fan){
		case 0:
			pwm_dma(pwmstate, 0);
			pwmstate=0;
			break;
		case 1:
			pwm_dma(pwmstate, 25);
			pwmstate=25;
			break;
		case 2:
			pwm_dma(pwmstate, 50);
			pwmstate=50;
			break;
		case 3:
			pwm_dma(pwmstate, 75);
			pwmstate=75;
			break;
		case 4:
			pwm_dma(pwmstate, 100);
			pwmstate=100;

			break;
		case 5:
			pwmstate=0;
			cont_fan=0;
			break;

		}
		uart_driver_send(&uart_driver,(uint8_t *)ack_message, sizeof(ack_message)-1);

	}else if (memcmp(rx_packet, close_door_cmd, sizeof (close_door_cmd)-1)==0){
		HAL_GPIO_WritePin(door_GPIO_Port, door_Pin, GPIO_PIN_RESET);
		uart_driver_send(&uart_driver,(uint8_t *)ack_message, sizeof(ack_message)-1);

	}else if (memcmp(rx_packet, open_door_cmd, sizeof (open_door_cmd)-1)==0){
		HAL_GPIO_WritePin(door_GPIO_Port, door_Pin, GPIO_PIN_SET);
		uart_driver_send(&uart_driver,(uint8_t *)ack_message, sizeof(ack_message)-1);

	}else if (memcmp(rx_packet, get_door_position_cmd, sizeof (get_door_position_cmd)-1)==0){
		if(HAL_GPIO_ReadPin(door_GPIO_Port, door_Pin)==1){
			uart_driver_send(&uart_driver,(uint8_t *)state_door_open, sizeof(state_door_open)-1);
		}
		if(HAL_GPIO_ReadPin(door_GPIO_Port, door_Pin)==0)
		{
			uart_driver_send(&uart_driver,(uint8_t *)state_door_close, sizeof(state_door_close)-1);
		}

	}else if (memcmp(rx_packet, get_fw_version_cmd, sizeof (get_fw_version_cmd)-1)==0){
		uart_driver_send(&uart_driver,(uint8_t *)ack_message6, sizeof(ack_message6)-1);

	}else if (memcmp(rx_packet, get_unit_tick_cmd, sizeof (get_unit_tick_cmd)-1)==0){

		uint32_t TICKS=HAL_GetTick();
		uint32_t ss = (TICKS/ticks_in_a_sec);
		uint32_t mm = (ss/secs_in_a_min);
		uint32_t hh = (mm/mins_in_a_hour);
		uint32_t dd = (hh/hours_in_a_day);

		float css = (float) (ss % secs_in_a_min);
		uint8_t cmm = (uint8_t) (mm % mins_in_a_hour);
		uint8_t chh = (uint8_t) (hh % hours_in_a_day);
		uint8_t cdd = (uint8_t) (dd);

		uint8_t datetime[DATATIME_LEN]={0x00};
		sprintf(datetime, "%02d:%02d:%02d:%02f",cdd,chh,cmm,css);

		uart_driver_send(&uart_driver,(uint8_t *)preamble1, sizeof(preamble1)-1);
		uart_driver_send(&uart_driver,(uint8_t *)datetime, sizeof(datetime));
		uart_driver_send(&uart_driver,(uint8_t *)posamble, sizeof(posamble)-1);

	}else if (memcmp(rx_packet, get_heater_state_cmd, sizeof (get_heater_state_cmd)-1)==0){
		if(HAL_GPIO_ReadPin(heater_GPIO_Port, heater_Pin)==1){
			uart_driver_send(&uart_driver,(uint8_t *)state_heater_on, sizeof(state_heater_on)-1);
		}
		if(HAL_GPIO_ReadPin(door_GPIO_Port, door_Pin)==0)
		{
			uart_driver_send(&uart_driver,(uint8_t *)state_heater_off, sizeof(state_heater_off)-1);
		}

		}
	else {
			uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message)-1);
		}
}
